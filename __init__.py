#!/usr/bin/env python
# -*- coding: utf-8 -*-

from docutils.parsers.rst.roles import set_classes
from docutils.parsers.rst.directives.admonitions import BaseAdmonition
from docutils import nodes

def depart_corr_node(self, node): pass
def visit_corr_html(self, node): pass

class Corr(BaseAdmonition):
    node_class = nodes.admonition

    def run(self): # overload d'une méthode de BaseAdmonition
        title_text = 'adm-corrige'
        
        set_classes(self.options)
        self.assert_has_content()
        text = '\n'.join(self.content)
        admonition_node = self.node_class(text, **self.options)
        self.add_name(admonition_node)
        textnodes, messages = self.state.inline_text(title_text,self.lineno)
        title = nodes.title(title_text, '', *textnodes)
        title.source, title.line = (self.state_machine.get_source_and_line(self.lineno))
        admonition_node += title
        admonition_node += messages
        admonition_node['classes'] += ['admonition-'+nodes.make_id(title_text)]
        self.state.nested_parse(self.content, self.content_offset,admonition_node)
        return [admonition_node]

def setup(app):
    app.add_node(Corr, html=(visit_corr_html, depart_corr_node))
    app.add_javascript('corr.js')
    app.add_directive('corr', Corr)